module.exports = {
	l: function(m){
		console.log(m);
	},
	twoDigits: function(d) {
	    if(0 <= d && d < 10) return "0" + d.toString();
	    if(-10 < d && d < 0) return "-0" + (-1*d).toString();
	    return d.toString();
	},
	toMysqlFormat: function(d) {
	    return d.getUTCFullYear() + "-" + this.twoDigits(1 + d.getUTCMonth()) + "-" + this.twoDigits(d.getUTCDate()) + " " + this.twoDigits(d.getUTCHours()) + ":" + this.twoDigits(d.getUTCMinutes()) + ":" + this.twoDigits(d.getUTCSeconds());
	}
};
