var DB_FILE = "hooks.db";
var DEBUG = false;

var fs = require("fs");
var cheerio = require('cheerio');
var request = require('request');
var sqlite3 = require('sqlite3').verbose();
var cron = require('cron');

var u = require('./utils.js');
var hooks = require('./hooks.js');

var exists = fs.existsSync(DB_FILE);

var db = new sqlite3.Database(DB_FILE);

function createTable(){
	var tn = "sites";
	db.run("CREATE TABLE IF NOT EXISTS " + tn + " (site TEXT, articleText TEXT, lastModifiedTime DATETIME)", function(err){
		if(err) throw err;
		u.l("Table " + tn + " created");
	});
}

function insertArticleIfNotExists(db, site, articleText, cb){
	db.all("SELECT rowId as id, * FROM sites where site = '" + site + "' and articleText = '" + articleText + "'", function(err, rows) {
		if(rows.length > 0) {
			cb(null, false, {
				"rows": rows
			});
		} else {
			var dateTime = u.toMysqlFormat(new Date());
			db.run("INSERT INTO sites VALUES ('" + site + "', '" + articleText + "', '" + dateTime + "')", function(err){
				var lastId = -1;
				var _err = null;

				if(err) _err = err;
				else lastId = this.lastID;
				
				cb(_err, true, {
					"lastId": lastId
				});
			});
		}
	});
}

function fetchProductHunt(){
	request('http://www.producthunt.com', function (error, response, html) {
	  	if (!error && response.statusCode == 200) {
	    	var $ = cheerio.load(html);
	    	var a = $('.post-item.v-category-tech.v-with-image').eq(0);
	    
	    	var $art = a.find('.post-item--text--name').eq(0);
	    	var articleText = $art.text();
	    	var articleUrl = "http://www.producthunt.com" + $art.attr('href');
	    	var voteCount = a.find('.post-vote-button--count').eq(0).text();
	    	var articleTagline = a.find('.post-item--text--tagline').eq(0).find("span").eq(0).text();

	    	u.l("\n\n");
	    	u.l("articleText: " + articleText);
	    	u.l("articleUrl: " + articleUrl);
	    	u.l("voteCount: " + voteCount);
	    	u.l("articleTagline: " + articleTagline);

	    	if(DEBUG === false) {
		    	insertArticleIfNotExists(db, "product_hunt", articleText, function(err, isNew, details){
					if(err) throw err;
					if(isNew == true) {
						u.l(details.lastId);
						var message = {message: articleText + " (" + voteCount+ " votes) (" + articleTagline + ")", url: articleUrl};
						hooks.sendNotification(message, 60);
					} else {
						u.l(articleText + " already exists");
						u.l(JSON.stringify(details.rows));
					}

					setTimeout(function(){
						fetchProductHunt();
					}, 900000)
				});
		    } else {
		    	setTimeout(function(){
					fetchProductHunt();
				}, 5000)
		    }
	  	}
	});
}


function fetchLaunchingNext(){
	request('http://www.launchingnext.com/', function (error, response, html) {
	  	if (!error && response.statusCode == 200) {
	    	var $ = cheerio.load(html);
	    	var $parent = $('.listing').eq(0);

	    	var articleText = $parent.find(" > p > a").eq(0).text();
	    	var articleUrl = $parent.find(" > a").eq(0).attr('href');
	    	var articleTagline = $parent.find(" > p").eq(1).text();
	   
	    	u.l("\n\n");
	    	u.l("articleText: " + articleText);
	    	u.l("articleUrl: " + articleUrl);
	    	u.l("articleTagline: " + articleTagline);

	    	if(DEBUG === false) {
		    	insertArticleIfNotExists(db, "launching_next", articleText, function(err, isNew, details){
					if(err) throw err;
					if(isNew == true) {
						u.l(details.lastId);
						var message = {message: articleText + " (" + articleTagline + ")", url: articleUrl};
						hooks.sendNotification(message, 96);
					} else {
						u.l(articleText + " already exists");
						u.l(JSON.stringify(details.rows));
					}

					setTimeout(function(){
						fetchLaunchingNext();
					}, 900000)
				});
			} else {
		    	setTimeout(function(){
					fetchLaunchingNext();
				}, 5000)
		    }
	  	}
	});
}


var jobs = [fetchProductHunt, fetchLaunchingNext];
// var cronJob = cron.job("0 */30 * * * *", function(){
// 	u.l('cron job iteration');
//     fetchProductHunt();
// }); 
// cronJob.start();
try {
	var nextJob = function(idx){
		if(idx < jobs.length) {
			setTimeout(function(){
				u.l("\nStarting job: " + idx);

				jobs[idx]();
				nextJob(idx + 1);
			}, 10000);
		} else {
			u.l("\n\nAll jobs started");
		}
	}

	nextJob(0);
} catch(err){
	console.log("\nERROR in starting job(s) :\n", err);
}

//--createTable();
