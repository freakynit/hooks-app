module.exports = {
	sendNotification: function(data, id) {
	  	var headers = {
	    	"Content-Type": "application/json",
	    	"Hooks-Authorization": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiTEJUV0lSQ3JRRyJ9.5eFyM4kV0u2NMrCTmLu-bDQQpXmb8t5PlsFnnpsc10A"
	  	};
	  
	  	var options = {
		    host: "api.gethooksapp.com",
		    port: 443,
		    path: "/v1/push/" + id,
		    method: "POST",
		    headers: headers
	  	};
	  
	  	var https = require('https');
	  	var req = https.request(options, function(res) {  
	    	res.on('data', function(data) {
		      	console.log("Response:");
		      	console.log(JSON.parse(data));
	    	});
	  	});
	  
	  	req.on('error', function(e) {
		    console.log("ERROR:");
		    console.log(e);
	  	});
	  
	  	req.write(JSON.stringify(data));
	  	req.end();
	}
}
